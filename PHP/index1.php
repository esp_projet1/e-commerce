<?php 
	include 'init.php';
	$sql = "SELECT * FROM produit WHERE vedette = 1";
	$vedette = $conn->query($sql);
	include 'head.php';
	include 'menu.php';
	include 'background.php'; 
	include 'annonce.php';
?>
	<div class="col-md-8">
		<div class="row">
			<h2 class="text-center" id="prod"><marquee>Futur Produits</marquee></h2>
			<?php
			$reqprep = $conn->prepare("$sql"); 
 			$reqprep ->execute(); 
 			foreach ($reqprep as $key => $test) { ?>
			<div class="col-md-3">
				<h4> <?= $test['titre']; ?></h4>
				<img src="<?= $test['image']; ?>" alt="<?= $test['titre']; ?>" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal <s><?= $test['prix_normal']; ?>Fcfa</s></p>
				<p class="price">Notre Prix: <?= $test['prix']; ?>Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" onclick="detailsmodal(<?= $test['id']; ?>)">Détails</button>
			</div>
			<?php } ?>
		</div>
	</div>

<?php
	include 'annoncepro.php';
	include 'pied.php';
?>
