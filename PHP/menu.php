<nav class="navbar navbar-default navbar-fixed-top" id="navbar">
	<div class="container">
		<a href="/index.php" class="navbar-brand" id="text"> Ambitieux Boutique</a>
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Homme<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Femme<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Garçon<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Fille<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>