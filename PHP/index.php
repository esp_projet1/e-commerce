<!DOCTYPE html>
<html>
<head>
	<title>Site E-commerce</title>
	<link rel="stylesheet" type="text/css" href="../Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../CSS/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scaleable=no">
	<script type="text/javascript" src="../JQuery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" id="navbar">
	<div class="container">
		<a href="/index.php" class="navbar-brand" id="text"> Ambitieux Boutique</a>
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Homme<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Femme<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Garçon<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="text">Fille<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">Shirts</a></li>
					<li><a href="#">Pantalon</a></li>
					<li><a href="#">Shoes</a></li>
					<li><a href="#">Accessoires</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>

<div id="background-image">
	<div id="image-1"></div>
	<div id="image-2"></div>
	<div id="image-3"></div>
</div>

<div class="container-fluid">
	<!-- Left bar -->
	<div class="col-md-2">Left Side Bar</div>

	<!-- main content -->
	<div class="col-md-8">
		<div class="row">
			<h2 class="text-center" id="prod"><marquee>Futur Produits</marquee></h2>
			<div class="col-md-3">
				<h4>Levis Jeans</h4>
				<img src="../Images/lewis.jpeg" alt="Lawis Jeans" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal <s>6000Fcfa</s></p>
				<p class="price">Notre Prix: 5000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Costume Homme</h4>
				<img src="../Images/costume2.jpeg" alt="Costume Homme" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal <s>25000Fcfa</s></p>
				<p class="price">Notre Prix: 15000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Costume Femme</h4>
				<img src="../Images/costumef.jpeg" alt="Costume Femme" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal <s>25000Fcfa</s></p>
				<p class="price">Notre Prix: 15000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Chaussure Homme</h4>
				<img src="../Images/chaussureh.jpeg" alt="Chaussure Homme" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal: <s>25000Fcfa</s></p>
				<p class="price">Notre Prix: 18000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			
			<div class="col-md-3">
				<h4>Chaussure Femme</h4>
				<img src="../Images/chaussuref.jpeg" alt="Chaussure Femme" class="img-thumb"/>
				<p class="list-price text-danger">Prix Normal <s>12000Fcfa</s></p>
				<p class="price">Notre Prix: 8000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Pochette Femme</h4>
				<img src="../Images/pochette.jpeg" alt="Pochette Femme" class="img-thumb" />
				<p class="list-price text-danger">Prix Normal <s>6000Fcfa</s></p>
				<p class="price">Notre Prix: 5000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Tenus Sportif</h4>
				<img src="../Images/sport1.jpeg" alt="Tenus Sportif" class="img-thumb" />
				<p class="list-price text-danger">Prix Normal <s>25000Fcfa</s></p>
				<p class="price">Notre Prix: 15000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
			<div class="col-md-3">
				<h4>Montre</h4>
				<img src="../Images/montre.jpeg" alt="Montre" class="img-thumb" />
				<p class="list-price text-danger">Prix Normal <s>6000Fcfa</s></p>
				<p class="price">Notre Prix: 5000Fcfa</p>
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#details-1">Détails</button>
			</div>
		</div>
	</div>

	<!-- right Sid Bar -->
	<div class="col-md-2"> Right Side Bar</div>
</div>

<footer class="text-center" id="footer">&copy; Copyright 2018-2019 Ambitieux Boutique</footer>

<!-- Détails Modal -->
<div class="modal fade details-1" id="details-1" tabindex="-1" role="dialog" aria-labelledby="details-1" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title text-center"> Levis Jeans</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-6">
							<div class="center-block">
								<img src="../Images/lewis.jpeg" alt="Levis Jeans" class="details img-responsive">
							</div>
						</div>
						<div class="col-sm-6">
							<h4>Détails</h4>
							<p>Ces jeans sont incroyables! ils sont jambe droite, très en forme et sexy, obtenez une paire avant que ça finisse</p>
							<hr>
							<p>Prix: 5000Fcfa</p>
							<p>Marque: Levis</p>
							<form action="add_cart.php" method="post">
								<div class="form-group">
									<div class="col-xs-3">
										<label for="quantity">Quantité:</label>
										<input type="text" class="form-control" id="quantity" name="quantity">
									</div><div class="col-xs-9"></div>
									<p>Disponible: 3</p>
								</div><br><br>
								<div class="form-group">
									<label for="size">Taille: </label>
									<select name="size" id="size" class="form-control">
										<option value=""></option>
										<option value="28">28</option>
										<option value="32">32</option>
										<option value="36">36</option>
									</select>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Close</button>
					<button class="btn btn-warning" type="submit"><span class="glyphicon glyphicon-shopping-cart">Add To Cart</span></button>
				</div>
			</div>
		</div>
	</div>
</div>


</body>
</html>