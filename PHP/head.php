<!DOCTYPE html>
<html>
<head>
	<title>Site E-commerce</title>
	<link rel="stylesheet" type="text/css" href="../Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../CSS/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scaleable=no">
	<script type="text/javascript" src="../JQuery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>