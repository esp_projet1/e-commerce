var myApp = angular.module('myApp', []);

myApp.controller("myController", function($scope) {
	console.log("In myController...");

	$scope.users = [
		{username: "Moimeme", fullname: "Abougueye", email:"Moimeme@exemple.com"}
		{username: "Sadikh", fullname: "Sadikhgueye", email:"Sadikh@exemple.com"}
		{username: "Ababacar", fullname: "Ababacargueye", email:"Ababacar@exemple.com"}
	];
});